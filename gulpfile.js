const gulp = require('gulp')
const browserSync = require('browser-sync')
const sass = require('gulp-sass')
const wait = require('gulp-wait')
const postCSS = require('gulp-postcss')
const autoprefixer = require('autoprefixer')

gulp.task('sass', () => {
  return gulp.src(['src/scss/main.scss'])
      .pipe(wait(1500))
      .pipe(sass())
      .pipe(postCSS([autoprefixer()]))
      .pipe(gulp.dest('build/css'))
      .pipe(browserSync.stream())
})

gulp.task('js', () => {
  return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js','node_modules/popper.js/dist/umd/popper.min.js','node_modules/lodash/lodash.min.js','/node_modules/validator/validator.min.js','src/js/*.js'])
      .pipe(gulp.dest('build/js'))
      .pipe(browserSync.stream())
})

gulp.task('assets', () => {
  return gulp.src(['src/assets/*/*'])
      .pipe(gulp.dest("build/assets"))
      .pipe(browserSync.stream());
});

gulp.task('serve', ['sass', 'js'], () => {
  browserSync.init({
    server: './',
    port: 3333
  })

  gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'], ['sass'])
  gulp.watch("*.html").on('change', browserSync.reload);
  gulp.watch("src/js/*.js", ['js']);
})

gulp.task('fonts', () => {
  return gulp.src('node_modules/font-awesome/fonts/*')
    .pipe(gulp.dest('build/fonts'))
})

// Move Font Awesome CSS to src/css
gulp.task('fa', () => {
  return gulp.src('node_modules/font-awesome/css/font-awesome.min.css')
    .pipe(gulp.dest('build/css'))
})

gulp.task('default', ['serve', 'fa', 'fonts', 'assets']);
'use strict'

SmoothScroll({
  animationTime: 800
})
SmoothScroll.init()

var $animation_elements = $('#services, #portfolio, #portfolio .fade-up, .contacts, #about .fade-up');
var $services = $('#services .mfade')
var $portfolioFadeUp = $('#portfolio .fade-up')
var $window = $(window);
var $navbar = $('.navbar')
var $navbarMobile = $('.navbar .mobile-show')
var $videoOverlay = $('.video-overlay')
var $scrollContent = $('.scroll-content')
var $portfolio = $('#portfolio')
var $heroVideo = $('.hero video')
var $whatMatters = $('#what-matters')
var $homeNavLink = $('a[href="#top"]')
var $servicesNavLink = $('a[href="#services"]')
var $portfolioNavLink = $('a[href="#portfolio"]')
var $contactsNavLink = $('a[href="#contacts"]')
var $contactForm = $('#contactForm')
var $serverOutput = $('#server-output')
var $logoImage = $('#logoImage')
var lastPosition = 0;
var ticking = false
var map
var mapScrollInitialized = false

if (isMobile()) {
  $logoImage.height("50")
}

// $('#cfSendButton').click(function () {
//   jivo_api.sendOfflineMessage({
//     "name": "John Smith",
//     "email": "email@example.com",
//     "phone": "+14084987855",
//     "description": "Description text",
//     "message": "Offline message"
//  }); 
//  $('#contactForm').modal('hide')
// })

// $('.btn-contact').click(function (e) {
//   e.preventDefault()
//   jivo_api.open()
// })

$('a[href*="#"]:not([href="#"])').click(function () {
  if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    $(this).blur()
    if (target.length) {
      if (target.is('#contacts')) {
        $('html, body').scrollTop(target.offset().top)
        return false;
      } else {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  }
});

if ($window.width() > 800) {
  $heroVideo.attr('autoplay', true).removeClass('d-none')
  $heroVideo.on('canplay loadedmetadata', function (e) {
    setTimeout(function() {
      $('.splash').addClass('d-none')
      $('body').removeClass('init-run')
    }, 1000);
    setTimeout(function() {
      $whatMatters.removeClass('faded')
    }, 1500);
  })
} else {
  setTimeout(function() {
    $('.splash').addClass('d-none')
    $('body').removeClass('init-run')
  }, 1000);
  setTimeout(function() {
    $whatMatters.removeClass('faded')
  }, 1500);
}

function isMobile() {
  try { document.createEvent("TouchEvent"); return true; }
  catch (e) { return false; }
}

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  if (window_top_position !== 0) {
    if ($navbar.hasClass('navbar-dark')) {
      $navbar.removeClass('navbar-dark').addClass('navbar-light bg-white')
      $navbarMobile.removeClass('text-white').addClass('text-info')
      if (!isMobile()) {
        $logoImage.animate({
          height: "50",
          duration: 1000
        })
      }
    }
  } else {
    if (!$navbar.hasClass('navbar-dark')) {
      $navbar.removeClass('navbar-light bg-white').addClass('navbar-dark')
      $navbarMobile.removeClass('text-info').addClass('text-white')
      if (!isMobile()) {
        $logoImage.animate({
          height: "80",
          duration: 1000
        })
      }
    }
  }

  if (window_top_position > 100) {
    if (!$videoOverlay.hasClass('darker')) {
      $videoOverlay.addClass('darker')
    }
  } else {
    if ($videoOverlay.hasClass('darker')) {
      $videoOverlay.removeClass('darker')
    }
  }

  if (!ticking) {
    ticking = true

    $.each($animation_elements, function () {
      var $element = $(this);
      var element_height = $element.outerHeight();
      var element_top_position = $element.offset().top;
      var element_bottom_position = (element_top_position + element_height);
      var downScroll = true
      var noScroll = true

      if (lastPosition - window_top_position < 0) {
        downScroll = true
        noScroll = false
      }
      if (lastPosition - window_top_position > 0) {
        downScroll = false
        noScroll = false
      }
      if (lastPosition - window_top_position === 0) {
        noScroll = true
      }
      lastPosition = window_top_position


      if ($element.is('#services')) {
        if ((element_bottom_position >= (window_top_position + 300)) &&
          (element_top_position <= (window_bottom_position - 300))) {
          if ($services.hasClass('faded')) {
            $services.removeClass('faded')
          }
        } else {
          if (!$services.hasClass('faded')) {
            $services.addClass('faded')
          }
        }
      }

      //  if ($element.is('#portfolio')) {
      //   if ((element_bottom_position >= (window_top_position + 300)) &&
      //     (element_top_position <= (window_bottom_position - 300))) {
      //     if ($scrollContent.hasClass('bg-light')) {
      //       $scrollContent.removeClass('bg-light').addClass('bg-dark text-white');
      //     }
      //   } else {
      //     if (!$scrollContent.hasClass('bg-light')) {
      //       $scrollContent.removeClass('bg-dark text-white').addClass('bg-light');
      //     }
      //   }
      // }

      if ($element.is('#portfolio')) {
        if ((element_bottom_position >= (window_top_position + 300)) &&
          (element_top_position <= (window_bottom_position - 300))) {
          if ($scrollContent.hasClass('bg-light')) {
            $scrollContent.removeClass('bg-light').addClass('bg-dark text-white');
          }
        } else {
          if (!$scrollContent.hasClass('bg-light')) {
            $scrollContent.removeClass('bg-dark text-white').addClass('bg-light');
          }
        }
      }

      if ($element.is('#about .fade-up')) {
        if ((element_bottom_position >= (window_top_position)) &&
          (element_top_position <= (window_bottom_position))) {
          if ($element.hasClass('faded')) {
            $element.removeClass('faded');
          }
        } else {
          if (!$element.hasClass('faded')) {
            $element.addClass('faded');
          }
        }
      }

      if ($element.is('#portfolio .fade-up')) {
        if ((element_bottom_position >= (window_top_position)) &&
          (element_top_position <= (window_bottom_position))) {
          if ($element.hasClass('faded')) {
            $element.removeClass('faded');
          }
        } else {
          if (!$element.hasClass('faded')) {
            $element.addClass('faded');
          }
        }
      }

      if ($element.is('.contacts')) {
        if ((element_bottom_position >= (window_top_position)) &&
          (element_top_position <= (window_bottom_position))) {
          if (!mapScrollInitialized) {
            initMap()
          }
        } else {
          mapScrollInitialized = false
        }
      }
    })
  }
  setTimeout(function() {
    ticking = false
  }, 300);
}

function initMap() {
  var mapOptions = {
    zoom: $window.width() > 600 ? 4 : 3,
    center: { lat: 35.202213, lng: -97.2366007 },
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    // scrollwheel: false,
    gestureHandling: 'cooperative',
    disableDefaultUI: true,
    // disableDoubleClickZoom: true,
    // draggable: true,
    // gestureHandling: 'none',
    backgroundColor: 'none',
    styles: [
      {
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#212121"
          }
        ]
      },
      {
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#212121"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#757575"
          },
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative.country",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#bdbdbd"
          }
        ]
      },
      {
        "featureType": "administrative.neighborhood",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#181818"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#1b1b1b"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#2c2c2c"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#8a8a8a"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#373737"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#3c3c3c"
          }
        ]
      },
      {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#4e4e4e"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "featureType": "transit",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#3d3d3d"
          }
        ]
      }
    ]
  };

  // Initialize the map with options (inside #map element)
  map = new google.maps.Map(document.getElementById('map'), mapOptions);

  // Markers
  var locations = [
    [
      25.773657,
      -80.1946987
    ],
    [
      40.777307,
      -73.9809487
    ],
    [
      33.930335,
      -118.4187567
    ]
  ];

  var markerIcon = {
    url: 'build/assets/map/map_marker.png',
    scaledSize: new google.maps.Size(40, 40),
    // origin: new google.maps.Point(0, 0), // used if icon is a part of sprite, indicates image position in sprite
    // anchor: new google.maps.Point(20,40) // lets offset the marker image
  };

  var marker
  var i = 0
  while (i < locations.length) {
    // Marker
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][0], locations[i][1]),
      map: map,
      url: 'https://www.google.com/maps/search/?api=1&query=' + locations[i][0] + ',' + locations[i][1],
      icon: markerIcon
    });

    google.maps.event.addListener(marker, 'click', function () {
      window.open(this.url, '_blank')
    });

    i++
  }

  // var marker = new google.maps.Marker({
  //   position: {lat:41.5000907, lng: -81.693648},
  //   map: map
  // });

  // var center = map.getCenter();

  // $("#contacts").on("show.bs.modal", function (e) {
  google.maps.event.trigger(map, "resize");
  mapScrollInitialized = true
  // map.setCenter(center);
  // });
}

$window.on('scroll resize', _.throttle(check_if_in_view, 16));
$window.on('resize', _.debounce(initMap, 400));
$window.trigger('scroll');
$window.trigger('resize');
$('#cfSendButton').one('click', contactFormClick)
// $('#navContacts, #navContactsMobile').click(function () {
//   $('.contacts').removeClass('hidden')
//   initMap()
//   setTimeout(() => {
//     $('#main-content').addClass('d-none')
//   }, 500);
// })
// $('#map-close').click(function () {
//   $('#main-content').removeClass('d-none')
//   setTimeout(() => {
//     $('.contacts').addClass('hidden')
//   }, 500);
// })

$contactForm.on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var message = button.data('message') // Extract info from data-* attributes
  if (message !== undefined) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var strs = message.split(',')
    var modal = $(this)
    modal.find('#cf-header-1').text(strs[0])
    modal.find('#cf-header-2').text(strs[1])
    modal.find('#cf-header-3').text(strs[2])
    modal.find('#cf-paragraph-1').text(strs[3])
    modal.find('#cf-message-text').val(strs[0])
  }
})

$contactForm.on('hidden.bs.modal', function (event) {
  $serverOutput.text('')
  $('#cf-email').val('')
  $('#cfSendButton').text('Send message').one('click', contactFormClick)
})

function contactFormClick(e) {
  e.preventDefault()
  e.stopPropagation()
  var emailIsValid = false
  var messageIsValid = false
  var $email = $('#cf-email')
  var $message = $('#cf-message-text')
  var self = $(this)

  if (validator.isEmail($email.val())) {
    emailIsValid = true
  } else {
    $email.addClass('is-invalid')
    $email.on('keyup', function () {
      $(this).removeClass('is-invalid').off()
    })
  }

  if (!validator.isEmpty($message.val())) {
    messageIsValid = true
  } else {
    $message.addClass('is-invalid')
    $message.on('keyup', function () {
      $(this).removeClass('is-invalid').off()
    })
  }

  if (emailIsValid && messageIsValid) {
    var data = {
      name: $('#cf-name').val(),
      email: $email.val(),
      phone: $('#cf-phone').val(),
      message: $message.val()
    }


    self.text('Sending...').attr('disabled', 'disabled')
    $.ajax({
      url: 'https://us-central1-ishor-1517242836781.cloudfunctions.net/contactForm',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(data),
      success: function (result) {
        $serverOutput.text('Message sent. Thank you').addClass('text-white small')
        self.text('Close').removeAttr('disabled').one('click', function (e) {
          e.preventDefault()
          e.stopPropagation()
          $contactForm.modal('hide')
        })
      },
      error: function (error) {
        $serverOutput.text('Some error occured. Try again later').addClass('text-white small')
        self.text('Close').removeAttr('disabled').one('click', function () {
          e.preventDefault()
          e.stopPropagation()
          $contactForm.modal('hide')
        })
      }
    })
  } else {
    self.one('click', contactFormClick)
  }
}

setTimeout(function() {
  $("#contactForm").modal("show")
}, 7000);

